Allows to create parent menu items acting as a container which simply points
to a page listing its children items.

USAGE
-----
Install the module as usual. 
See http://drupal.org/documentation/install/modules-themes/modules-7
Navigate to admin/build/menu and click on the menu you want to customize.
Add a new menu link or edit an existing link
Enter <children> as the link path.

Similar module
--------------
Menu First Children module http://drupal.org/project/menu_firstchild
Many thanks to anrikun. 
His module save me quite some time in developing this one.
